        <div class="content-w">
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <div class="element-box">
                    <div class="steps-w">

                      <div class="step-triggers">
                        <a class="step-trigger" href="#stepContent1">Penyelesaian</a></a>
                      </div>
                      <form action="/auditor/ncr/insert" method="post">
                      <div class="form-group">
                            <label> Investigasi Penyebab Ketidaksesuaian </label><textarea class="form-control" rows="3"></textarea>
                          </div>
                          <div class="form-group">
                            <label> Tindakan Perbaikan yang Diambil </label><textarea class="form-control" rows="3"></textarea>
                          </div>
                          <div class="form-group">
                            <label> Lampiran </label><textarea class="form-control" rows="3"></textarea>
                          </div>
                          
                        </div>
                        
                        <?php
                                  $unit=$data['unit_kerja'];
                                  $klausul=$data['klausul_iso'];
                                  $auditee=$data['auditee'];
                        ?>
                        {{ csrf_field() }}

                            
                              <div class="form-group">
                                <label for=""> Tanggal Rencana Penyelesaian :</label><input class="form-control" type="date" id="start" name="tgl_mulai"
                                      value=""/>
                              </div>
                            
                        <div class="form-buttons-w text-right">
                            <input class="btn btn-primary step-trigger-btn" type="submit" name="submit" value='Submit' />
                          </div>
                        </form>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>