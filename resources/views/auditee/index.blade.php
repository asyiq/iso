<!-- Header -->
@include('header')

<div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">
		<!-- Sidebar -->
		@include('auditee.sidebar')

		<!-- Content -->
		@include('auditee.rencana')
		</div>
      <div class="display-type"></div>
 </div>
 
<!-- Footer -->
@include('footer')