        <div class="content-w">
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      PERIODE AUDIT
                    </h6>
                    <div class="element-box-tp">
                      @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                      <form action="{{ url('admin/auditor/submit')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="steps-w">
                          <div class="step-contents">
                            <div class="step-content active" id="stepContent1">
                              <div class="col-sm-4">
                                <label for=""> Periode Ke :</label>
                                <select name="periode" class="form-control select2" id="">
                                  @foreach($periode as $item)
                                    <option value="{{ $item->periode_id }}">{{ $item->periode_ke }}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-sm-4">
                                

                                  <div class="form-buttons-w text-right">
                                    <button class="btn btn-primary" type="submit">Selesai</button>
                                  </div>
                              </div>
                              <div class="controls-above-table">
                              </div>
                            </div>
                          </div>
                        </div>
                      <div class="table-responsive">
                        <table class="table table-bordered table-lg table-v2 table-striped">
                          <thead>
                            <tr>
                              <th class="col-sm-1">
                                ID
                              </th>
                              <th class="col-sm-3"> 
                                NIP
                              </th>
                              <th class="col-sm-3">
                                Nama
                              </th>
                              
                              <th class="col-sm-1">
                                Lead Auditor
                              </th>
                              <th class="col-sm-1">
                                Auditor
                              </th>
                              
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $no = 1;
                            ?>
                            @foreach($users as $key => $data)
                            <tr style="text-align: center;">
                              <td style="text-align: center;"> 
                                {{$no++}}
                              </td>
                              <td>
                                {{$data->nip}}
                              </td>
                              <td>
                                {{$data ->name}}
                              </td>
                              <td>
                                <label class="switch">
                                  <input type="checkbox" value="1" name="leader" class="leader-switch">
                                  <span class="slider round"></span>
                                </label>
                              </td>
                              <td>
                                <label class="switch">
                                  <input type="checkbox" name="check[]" value="{{ $data->id }}">
                                  <span class="slider round"></span>
                                </label>
                              </td>
                            </tr>   
                          </tbody>
                          @endforeach
                      </form>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        @section('script')
          <script type="text/javascript">
            let leader = 0;
            $('.leader-switch').on('change', function () {
              
              if ($(this).prop('checked')) {

                  $('.leader-switch').each( function () {
                    console.log(this.checked)
                    if (this.checked == false) {
                      this.disabled = true
                    }                
                  })
              } else {
                 $('.leader-switch').each( function () {
                    this.disabled = false                
                  })
              }
            })

          </script>
        @endsection
