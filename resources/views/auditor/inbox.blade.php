<div class="content-w">
          <div class="content-i">
            <div class="content-box"><div class="app-email-w">
                <div class="app-email-i">
                  <div class="ae-side-menu">
                    <div class="aem-head">
                      <a class="ae-side-menu-toggler" href="#"><i class="os-icon os-icon-hamburger-menu-2"></i></a>
                    </div>
                    <ul class="ae-main-menu">
                      <li>
                        <a href="#"><i class="os-icon os-icon-ui-44"></i><span>New</span></a>
                      </li>
                      <li class="active">
                        <a href="#"><i class="os-icon os-icon-phone-21"></i><span>Inbox</span></a>
                      </li>
                      <li lass="active">
                        <a href="#"><i class="os-icon os-icon-ui-92"></i><span>Sent</span></a>
                      </li>
                    </ul>
                  </div>
                  <div class="ae-list-w">
                    <div class="ael-head">
                      <div class="actions-left">
                        <select>
                          <option>
                            Sort by periode
                          </option>
                        </select>
                      </div>
                      <div class="actions-right">
                        <a href="#"><i class="os-icon os-icon-ui-37"></i></a>
                        <a href="#"><i class="os-icon os-icon-grid-18"></i></a>
                      </div>
                    </div>
                    <div class="ae-list">
                      <div class="ae-item with-status active status-green">
                        <div class="aei-content">
                          <div class="aei-timestamp">
                            1:25pm
                          </div>
                          <h6 class="aei-title">
                            James Morgan
                          </h6>
                          <div class="aei-sub-title">
                            NON CONFORMITY REPORT
                          </div>
                          <div class="aei-text">
                            When the equation, first to ability the forwards, the a but travelling
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="ae-content-w">
                    <div class="aec-head">
                      <div class="actions-left">
                      </div>
                      <div class="actions-right">
                        <a href="#"><i class="os-icon os-icon-ui-15"></i></a>
                        <div class="user-avatar">
                          <img alt="" src="img/avatar3.jpg">
                        </div>
                      </div>
                    </div>
                    <div class="ae-content">
                      <div class="aec-full-message-w">
                        <div class="aec-full-message">
                          <div class="message-head">
                            <div class="user-w with-status status-green">
                              <div class="user-name">
                                <h6 class="user-title">
                                  John Mayers
                                </h6>
                                <div class="user-role">
                                  Account Manager<span>< john@solutions.com ></span>
                                </div>
                              </div>
                            </div>
                            <div class="message-info">
                              January 12th, 2017<br>1:24pm
                            </div>
                          </div>
                          <div class="message-content">
                            Hi Maria,<br><br>No:<br>Unit Kerja:<br>Lingkup Audit:<br>Kategori Ketidaksesuaian:<br>Klausul ISO:<br><br>Uraian Ketidaksesuaian : <br>Bukti Ketidaksesuaian:<br><br>Regards,<br>Mike Mayers
                          </div>
                        </div>
                      </div>
                      <div class="aec-reply buttons-w">
                        <div class="actions-right">
                            <a class="btn btn-success" href="auditor/ncr"><i class="os-icon os-icon-mail-18"></i><span>Buat Baru</span></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</div>