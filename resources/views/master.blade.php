<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" >
        <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" type="text/css">

        <style>
            html, body {
                margin: 0;
                padding: 0;
                width: 100%;
                background-color: #17a2b8;
                text-align: center;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            .label-form-login {
                float: left ;
                margin: 0px ;
                font-family: sans-serif;
                font-size: 17px;
             }
            .btn-login{
                width: 100%;
                font-family: 'sans-serif';
                background-color: royalblue;
            }

            .form-login{
                float: none ;
                margin: auto;
                font-family: fantasy;
            }


            #login .container #login-row #login-column #login-box {
                border: 3px solid #9C9C9C;
                border-radius: 3%;
                background-color: #EAEAEA;
            }
            #login .container #login-row #login-column #login-box #login-form {
                padding: 20px;
            }

            #icon-login{
                max-width: 100px;
                margin-bottom: 20px;
            }
            #login .container #login-row #login-column #login-box #login-form #register-link {
                margin-top: -85px;
            }

            .col-icon-login{
                float: none !important;
                margin: auto;
                margin-right: 0px;
                margin-left: 16%;
                padding: 0px!important;
            }

            .col-header-login{
                float: left !important;
                margin: auto;
                margin-left: 0px;
                text-align: left;
            }

        </style>
        <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">
    </head>
    <body>
    <div class="container" >
        @yield('content')
    </div>
    </body>
</html>
