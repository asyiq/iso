<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Periode;

class periodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrator.indexperiode');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'tgl_mulai' => 'required',
            'tgl_akhir' => 'required',
            'periode_ke' => 'required|unique:periode',
        ]);
        

        if ($validator->fails()) {
            return redirect('admin/periode')
                        ->withErrors($validator)
                        ->withInput();
        }

        $insert = Periode::insert($request->except('_token'));

 
        if ($insert) {
            return 'Insert berhasil';
        } else {
            return 'Insert gagal';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewPeriode(Request $request, $id){
        $periode = Periode::with(["auditor"=> function($query){
            return $query->with("user")->get();
        }])
        ->where('periode_id','=',$id)->get();
        // dd($periode);

        return response()->json($periode);

        // return view('administrator.indexauditor',compact('periode'));
    }
    public function deletePeriode(Request $request, $id){
        $periode = Periode::where('periode_id','=',$id)->delete();
        return $periode;
    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
