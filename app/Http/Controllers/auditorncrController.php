<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\unit_kerja;
use App\klausul_iso;
use App\auditee;
use App\ncr;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class auditorncrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view(){
        return view('auditor.indexinbox');
    }
    public function show()
    {
        $data['unit_kerja'] = unit_kerja::all();
        $data['klausul_iso'] = klausul_iso::all();
        $data['auditee'] = auditee::all();
        $data['ncr'] = ncr::all();

        return view('auditor.index')->with(compact('data'));
        // return $select;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
