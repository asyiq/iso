<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = DB::table('users')->insert([
            'name' => 'admin',
            'nip' => '1234567',
            'email' => 'admin@gmail.com',
            'password' => \Hash::make('sandiaman'),
            'role_id_user' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }
}
